An MPESA RESTFUL API

# Technology Stacks

- Python 3.5
- Django 1.11
- Django Rest Framework 3.0
- JWT

other dependancies you will find them in requirements.txt

Further information about MPESA API you will find in the [safaricom documentation](https://developer.safaricom.co.ke/docs#m-pesa-result-codes)